class Room
{
private:
    int pirPin;
    int ledPin;
    int ldrPin;
    int minLightLevel;
    unsigned long lightDelay;
    unsigned long lowIn;
    int lightLevel;
    boolean lockLow = true;
    boolean automatic = false;
    boolean takeLowTime;

public:
    Room(int pir, int led, int ldr, int _minLightLevel, boolean _automatic, unsigned long _lightDelay)
    {
        pirPin = pir;
        pinMode(pirPin, INPUT);
        ledPin = led;
        pinMode(ledPin, OUTPUT);
        ldrPin = ldr;
        pinMode(ldrPin, INPUT);
        minLightLevel = _minLightLevel;
        automatic = _automatic;
        lightLevel = LightLevel();
        lightDelay = _lightDelay;
    }
    void setPirPin(int pin)
    {
        pirPin = pin;
    }
    int getPirPin()
    {
        return pirPin;
    }
    void setLedPin(int pin)
    {
        ledPin = pin;
    }
    int getLedPin()
    {
        return ledPin;
    }
    void setLdrPin(int pin)
    {
        ldrPin = pin;
    }
    int getLdrPin()
    {
        return ldrPin;
    }
    void setMinLightLevel(int level)
    {
        minLightLevel = level;
    }
    int getMinLightLevel()
    {
        return minLightLevel;
    }
    void setAutomatic(boolean _automatic)
    {
        automatic = _automatic;
    }
    boolean getAutomatic()
    {
        return automatic;
    }
    void setLightDelay(unsigned long _lightDelay)
    {
        lightDelay = _lightDelay;
    }
    long getLightDelay()
    {
        return lightDelay;
    }
    int LightLevel();
    void turnLightOn();
};

int Room::LightLevel()
{
    const float COEFICIENTE = 100.0 / 4095;
    float _lightlevel = analogRead(ldrPin) * COEFICIENTE;
    int lightLevel_ = round(_lightlevel);
    return lightLevel_;
}

void Room::turnLightOn()
{
    lightLevel = LightLevel();
    if (digitalRead(pirPin) == HIGH && lightLevel < minLightLevel && automatic)
    {
        digitalWrite(ledPin, HIGH);
        if (lockLow)
        {
            lockLow = false;
            delay(50);
        }
        takeLowTime = true;
    }
    if (digitalRead(pirPin) == LOW)
    {
        if (takeLowTime)
        {
            lowIn = millis();
            takeLowTime = false;
        }
        if (!lockLow && millis() - lowIn > lightDelay)
        {
            lockLow = true;
            digitalWrite(ledPin, LOW);
            delay(50);
        }
    }
}