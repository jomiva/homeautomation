// Parsing config.json document from SPIFFS
void loadConfig(JsonDocument &doc, String f_path)
{
    File configFile = SPIFFS.open(f_path, "r");
    deserializeJson(doc, configFile);
    configFile.close();
}

void saveConfig(JsonDocument &doc, String f_path)
{
    SPIFFS.remove(f_path);
    File file = SPIFFS.open(f_path, "w");
    if (!file)
    {
        Serial.println(F("Failed to create file"));
        return;
    }
    serializeJson(doc, file);
    file.close();
}

void backup(Room &_room, JsonDocument &_doc)
{
    _room.setPirPin(_doc["pirpin"]);
    _room.setLedPin(_doc["ledpin"]);
    _room.setLdrPin(_doc["ldrpin"]);
    _room.setMinLightLevel(_doc["minlightlevel"]);
    _room.setLightDelay(_doc["lightdelay"]);
    _room.setAutomatic(_doc["automatic"]);
}

void backupattr(int _minlightlevel, unsigned long _delay, boolean _automatic, Room &_room, JsonDocument &_doc)
{
    Serial.println(_minlightlevel);
    _doc["minlightlevel"] = _minlightlevel;
    _room.setMinLightLevel(_doc["minlightlevel"]);

    Serial.println(_delay);
    _doc["lightdelay"] = _delay;
    _room.setLightDelay(_doc["lightdelay"]);

    Serial.println(_automatic);
    _doc["automatic"] = _automatic;
    _room.setAutomatic(_doc["automatic"]);
}