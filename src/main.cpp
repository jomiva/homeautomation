#include <Arduino.h>
#include <WiFi.h>
#include <SPIFFS.h>
#include <ESPAsyncWebServer.h>
#include <WebSocketsServer.h>
#include <ESP32Servo.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <ArduinoJson.h>
#include <ESPmDNS.h>
#include <SmartRoom.h>
#include <DataJson.h>
#include <HCSR04.h>
#include "webh/required.htm.gz.h"
#include "webh/required.css.gz.h"
#include "webh/required.js.gz.h"
#include "webh/required.woff.gz.h"

const int capacity = JSON_OBJECT_SIZE(5) + 90;
const int capacityroom = JSON_OBJECT_SIZE(6) + 70;
const int capacityuser = JSON_OBJECT_SIZE(1) + 2 * JSON_OBJECT_SIZE(2) + 136;
const int HTTP_PORT = 8888;
const int SERVOPIN = 23;
const int BUZZER = 21;
const int extractor = 4;
boolean wififlag = true;
boolean alarma_seguridad = true;
boolean garage_auto;
IPAddress ip_server;
unsigned long startMillis;
unsigned long currentMillis;
#define DHTPIN 22
#define DHTTYPE DHT11

UltraSonicDistanceSensor sr04(14, 25);

// Globals
AsyncWebServer server(HTTP_PORT);
WebSocketsServer webSocket = WebSocketsServer(1337);
Servo myservo;
// Servo myservoporton;
DHT dht(DHTPIN, DHTTYPE);
Room cocina(12, 13, 36, 50, true, 5000);
Room cuarto(18, 19, 39, 50, true, 5000);
Room bano(27, 26, 34, 50, true, 5000);
Room sala(33, 32, 35, 50, true, 5000);
// Callback: send homepage

void message_deco(uint8_t _client, uint8_t *payload, size_t length)
{
  String incoming_message;
  for (size_t i = 0; i < length; i++)
  {
    incoming_message += (char)payload[i];
  }
  Serial.println(incoming_message);
  String room_target = incoming_message.substring(0, 2);
  Serial.println(room_target);
  boolean _auto = incoming_message.substring(2, 3).toInt();
  Serial.println(_auto);
  unsigned long newtimedelay = incoming_message.substring(3, 5).toInt() * 1000;
  Serial.println(newtimedelay);
  int newminlightlevel = incoming_message.substring(5).toInt();
  Serial.println(newminlightlevel);
  if (room_target == "co")
  {
    StaticJsonDocument<capacityroom> cocinajson;
    loadConfig(cocinajson, "/cocinaconfig.json");
    backupattr(newminlightlevel, newtimedelay, _auto, cocina, cocinajson);
    saveConfig(cocinajson, "/cocinaconfig.json");
    webSocket.sendTXT(_client, "succesfully");
    webSocket.broadcastTXT("update-luces");
    return;
  }
  else if (room_target == "sa")
  {
    StaticJsonDocument<capacityroom> salajson;
    loadConfig(salajson, "/salaconfig.json");
    backupattr(newminlightlevel, newtimedelay, _auto, sala, salajson);
    saveConfig(salajson, "/salaconfig.json");
    webSocket.sendTXT(_client, "succesfully");
    webSocket.broadcastTXT("update-luces");
    return;
  }
  else if (room_target == "cu")
  {
    StaticJsonDocument<capacityroom> cuartojson;
    loadConfig(cuartojson, "/cuartoconfig.json");
    backupattr(newminlightlevel, newtimedelay, _auto, cuarto, cuartojson);
    saveConfig(cuartojson, "/cuartoconfig.json");
    webSocket.sendTXT(_client, "succesfully");
    webSocket.broadcastTXT("update-luces");
    return;
  }
  else if (room_target == "ba")
  {
    StaticJsonDocument<capacityroom> banojson;
    loadConfig(banojson, "/banoconfig.json");
    backupattr(newminlightlevel, newtimedelay, _auto, bano, banojson);
    saveConfig(banojson, "/banoconfig.json");
    webSocket.sendTXT(_client, "succesfully");
    webSocket.broadcastTXT("update-luces");
    return;
  }
  else if (incoming_message.length() > 20)
  {
    StaticJsonDocument<capacityuser> userjson;
    deserializeJson(userjson, incoming_message);
    Serial.println("se ejecuto user");
    saveConfig(userjson, "/users.json");
    return;
  }
  else
  {
    return;
  }
  return;
}

void onWebSocketEvent(uint8_t client_num,
                      WStype_t type,
                      uint8_t *payload,
                      size_t length)
{

  // Figure out the type of WebSocket event
  switch (type)
  {

  // Client has disconnected
  case WStype_DISCONNECTED:
    Serial.printf("[%u] Disconnected!\n", client_num);
    break;

  // New client has connected
  case WStype_CONNECTED:
  {
    IPAddress ip = webSocket.remoteIP(client_num);
    Serial.printf("[%u] Connection from ", client_num);
    Serial.println(ip.toString());
  }
  break;

  // Handle text messages from client
  case WStype_TEXT:

    // Print out raw message
    Serial.printf("[%u] Received text: %s\n", client_num, payload);
    if (!strcmp((char *)payload, "cocinaluzon"))
    {
      digitalWrite(cocina.getLedPin(), HIGH);
      return;
    }
    else if (!strcmp((char *)payload, "cocinaluzoff"))
    {
      digitalWrite(cocina.getLedPin(), LOW);
      return;
    }
    else if (!strcmp((char *)payload, "cuartoluzon"))
    {
      digitalWrite(cuarto.getLedPin(), HIGH);
      return;
    }
    else if (!strcmp((char *)payload, "cuartoluzoff"))
    {
      digitalWrite(cuarto.getLedPin(), LOW);
      return;
    }
    else if (!strcmp((char *)payload, "bañoluzon"))
    {
      digitalWrite(bano.getLedPin(), HIGH);
      return;
    }
    else if (!strcmp((char *)payload, "bañoluzoff"))
    {
      digitalWrite(bano.getLedPin(), LOW);
      return;
    }
    else if (!strcmp((char *)payload, "salaluzon"))
    {
      digitalWrite(sala.getLedPin(), HIGH);
      return;
    }
    else if (!strcmp((char *)payload, "salaluzoff"))
    {
      digitalWrite(sala.getLedPin(), LOW);
      return;
    }
    else if (!strcmp((char *)payload, "extractoron"))
    {
      digitalWrite(extractor, HIGH);
      return;
    }
    else if (!strcmp((char *)payload, "extractoroff"))
    {
      digitalWrite(extractor, LOW);
      return;
    }
    else if (!strcmp((char *)payload, "mute"))
    {
      digitalWrite(BUZZER, LOW);
      return;
    }
    else if (!strcmp((char *)payload, "garageauto"))
    {
      garage_auto = true;
      myservo.write(110);
      startMillis = millis();
      return;
    }
    else if (!strcmp((char *)payload, "garageopen"))
    {
      myservo.write(110);
      return;
    }
    else if (!strcmp((char *)payload, "garageclose"))
    {
      myservo.write(0);
      return;
    }
    else if (!strcmp((char *)payload, "alarma-seguridad-toggle-true"))
    {
      StaticJsonDocument<capacity> alarmajson;
      loadConfig(alarmajson, "/alarma.json");
      alarma_seguridad = true;
      alarmajson["seguridad"]["activado"] = alarma_seguridad;
      saveConfig(alarmajson, "/alarma.json");
      return;
    }
    else if (!strcmp((char *)payload, "alarma-seguridad-toggle-false"))
    {
      StaticJsonDocument<capacity> alarmajson;
      loadConfig(alarmajson, "/alarma.json");
      alarma_seguridad = false;
      alarmajson["seguridad"]["activado"] = alarma_seguridad;
      saveConfig(alarmajson, "/alarma.json");
      return;
    }
    else
    {
      message_deco(client_num, payload, length);
      Serial.printf("[%u] Received text: %s\n", client_num, payload);
      return;
    }
    break;

  // For everything else: do nothing
  case WStype_BIN:
  case WStype_ERROR:
  case WStype_FRAGMENT_TEXT_START:
  case WStype_FRAGMENT_BIN_START:
  case WStype_FRAGMENT:
  case WStype_FRAGMENT_FIN:
  default:
    break;
  }
}

String readDHTTemperature()
{
  float t = dht.readTemperature();
  if (isnan(t))
  {
    Serial.println("Failed to read from DHT sensor!");
    return "--";
  }
  else
  {
    Serial.println(t);
    return String(t);
  }
}

String readDHTHumidity()
{
  float h = dht.readHumidity();
  if (isnan(h))
  {
    Serial.println("Failed to read from DHT sensor!");
    return "--";
  }
  else
  {
    Serial.println(h);
    return String(h);
  }
}

String readLightLevel(Room &_room)
{
  int light_level = _room.LightLevel();
  Serial.println(light_level);
  String getLightLevel = String(light_level, DEC);
  Serial.println(getLightLevel);
  return getLightLevel;
}

/***********************************************************
 * Main
 */

// DNSServer dnsServer;
// IPAddress local_IP(192, 168, 0, 215);
// IPAddress gateway(192, 168, 0, 1);
// IPAddress subnet(255, 255, 0, 0);

void sincronizarPIR()
{
  const int CALIBRATION = 30;
  Serial.println("Calibrando Sensor PIR");
  for (int i = 0; i < CALIBRATION; i++)
  {
    Serial.print(".");
    if (i % 10 == 0)
    {
      // digitalWrite(BUZZER, HIGH);
    }
    delay(1000);
    digitalWrite(BUZZER, LOW);
  }
  Serial.println("SENSOR ACTIVE");
}

void WifiConnect()
{
  StaticJsonDocument<capacity> wifi;
  loadConfig(wifi, "/wificonfig.json");
  if (!wififlag)
  {
    ESP.restart();
  }

  if (wifi["mode"] == "STA")
  {
    const char *ssid = wifi["ssid"];
    const char *password = wifi["password"];
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    unsigned long startTime = millis();
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);
      Serial.println(".");
      digitalWrite(2, !digitalRead(2));
      if (millis() - startTime > 7000)
        break;
    }
  }

  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("Connected to the WiFi network");
    Serial.println(WiFi.localIP());
    ip_server = WiFi.localIP();
    digitalWrite(2, HIGH);
  }
  else
  {
    const char *_ssid = wifi["myssid"];
    const char *_password = wifi["mypassword"];
    WiFi.mode(WIFI_AP);
    WiFi.softAP(_ssid, _password);
    ip_server = WiFi.softAPIP();
    digitalWrite(2, LOW);
  }
  Serial.println("");
  WiFi.printDiag(Serial);
}

void handleSettingsWifiUpdate(String data)
{
  StaticJsonDocument<capacity> wifi;
  loadConfig(wifi, "/wificonfig.json");
  deserializeJson(wifi, data);
  saveConfig(wifi, "/wificonfig.json");
}

void loadDataOnSetupCocina()
{
  StaticJsonDocument<capacityroom> cocinajson;
  loadConfig(cocinajson, "/cocinaconfig.json");
  backup(cocina, cocinajson);
}

void loadDataOnSetupBano()
{
  StaticJsonDocument<capacityroom> banojson;
  loadConfig(banojson, "/banoconfig.json");
  backup(bano, banojson);
}

void loadDataOnSetupSala()
{
  StaticJsonDocument<capacityroom> salajson;
  loadConfig(salajson, "/salaconfig.json");
  backup(sala, salajson);
}

void loadDataOnSetupCuarto()
{
  StaticJsonDocument<capacityroom> cuartojson;
  loadConfig(cuartojson, "/cuartoconfig.json");
  backup(cuarto, cuartojson);
}

void loadDataOnSetupAlarm()
{
  StaticJsonDocument<capacity> alarmajson;
  loadConfig(alarmajson, "/alarma.json");
  alarma_seguridad = alarmajson["seguridad"]["activado"];
}

void TriggerAlarm()
{
  int distance = int(sr04.measureDistanceCm());
  if (alarma_seguridad && distance <= 14 && distance >= 10)
  {
    digitalWrite(BUZZER, HIGH);
    Serial.println(distance);
    delay(100);
  }
  // delay(5000);
}

void setup()
{
  Serial.begin(115200);
  pinMode(2, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  sincronizarPIR();
  delay(50);
  myservo.setPeriodHertz(50); // Standard 50hz servo
  myservo.attach(SERVOPIN, 500, 2400);
  // Make sure we can read the file system
  if (!SPIFFS.begin())
  {
    Serial.println("Error mounting SPIFFS");
    while (1)
      ;
  }
  loadDataOnSetupCocina();
  loadDataOnSetupSala();
  loadDataOnSetupCuarto();
  loadDataOnSetupBano();
  loadDataOnSetupAlarm();
  // Start STA
  // if (!WiFi.config(local_IP, gateway, subnet))
  // {
  //   Serial.println("STA Failed to configure");
  // }

  // StaticJsonDocument<capacity> wifi;
  // loadConfig(wifi, "/wificonfig.json");

  // const char *ssid = wifi["ssid"];
  // const char *password = wifi["password"];
  // WiFi.begin(ssid, password);

  // // Print our IP address
  // while (WiFi.status() != WL_CONNECTED)
  // {
  //   delay(500);
  //   Serial.println("Connecting to WiFi..");
  // }

  WifiConnect();
  wififlag = false;
  // Serial.println("Connected to the WiFi network");
  // Serial.println(WiFi.localIP());

  // dnsServer.start(HTTP_PORT,"*",WiFi.localIP());
  if (!MDNS.begin("smarthouse"))
  {
    Serial.println("Error setting up MDNS responder!");
    while (1)
    {
      delay(1000);
    }
  }
  Serial.println("mDNS responder started");

  server.on("/index.html", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncWebServerResponse *response = request->beginResponse_P(200, "text/html", required_htm_gz, required_htm_gz_len);
    response->addHeader("Content-Encoding", "gzip");
    request->send(response);
  });

  server.on("/cocinaconfig.json", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/cocinaconfig.json");
  });
  server.on("/salaconfig.json", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/salaconfig.json");
  });
  server.on("/cuartoconfig.json", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/cuartoconfig.json");
  });
  server.on("/banoconfig.json", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/banoconfig.json");
  });
  server.on("/wificonfig.json", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/wificonfig.json");
  });
  server.on("/users.json", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/users.json");
  });
  server.on("/alarma.json", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/alarma.json");
  });

  // Handle requests for pages that do not exist
  server.on("/mini.css", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncWebServerResponse *response = request->beginResponse_P(200, "text/css", required_css_gz, required_css_gz_len);
    response->addHeader("Content-Encoding", "gzip");
    request->send(response);
  });

  server.on("/bundle.js", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncWebServerResponse *response = request->beginResponse_P(200, "text/javascript", required_js_gz, required_js_gz_len);
    response->addHeader("Content-Encoding", "gzip");
    request->send(response);
  });

  server.on("/fonts/fa-solid-900.woff2", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncWebServerResponse *response = request->beginResponse_P(200, "font/woff", required_woff_gz, required_woff_gz_len);
    response->addHeader("Content-Encoding", "gzip");
    request->send(response);
  });

  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/plain", readDHTTemperature().c_str());
  });

  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/plain", readDHTHumidity().c_str());
  });
  server.on("/lightlevel-cocina", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/plain", readLightLevel(cocina).c_str());
  });

  server.on("/lightlevel-sala", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/plain", readLightLevel(sala).c_str());
  });

  server.on("/lightlevel-cuarto", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/plain", readLightLevel(cuarto).c_str());
  });

  server.on("/lightlevel-bano", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/plain", readLightLevel(bano).c_str());
  });

  server.on("/server-host", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/plain", ip_server.toString().c_str());
  });

  server.on(
      "/settings",
      HTTP_POST,
      [](AsyncWebServerRequest *request) {},
      NULL,
      [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
        String body = "";
        for (size_t i = 0; i < len; i++)
        {
          body += (char)data[i];
        }
        // Serial.println(body);
        handleSettingsWifiUpdate(body);
        WifiConnect();
        request->send(200, "application/json", "{\"status\":\"ok\"}");
      });

  server.onNotFound([](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/404.html", "text/html");
  });
  server.rewrite("/", "/index.html");
  server.begin();
  // Start web server
  dht.begin();

  // Start WebSocket server and assign callback
  webSocket.begin();
  webSocket.onEvent(onWebSocketEvent);
  MDNS.addService("_http", "_tcp", HTTP_PORT);
  // MDNS.addService("ws", "tcp", 1337);
}

void loop()
{
  TriggerAlarm();
  sala.turnLightOn();
  bano.turnLightOn();
  cuarto.turnLightOn();
  cocina.turnLightOn();
  if (garage_auto)
  {
    currentMillis = millis();
    if (currentMillis - startMillis >= 5000)
    {
      myservo.write(0);
      garage_auto = false;
    }
  }
  webSocket.loop();
}
